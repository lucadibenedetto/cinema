﻿using System; 
using System.Collections.Generic;
using System.IO;
using MySql.Data.MySqlClient;
using CsvHelper;

namespace scritturasuDB
{
    class Program
    {
        static void Main(string[] args)
        {
            string cs = @"server=localhost;userid=luca;password=Lucaroma01;database=analisi_programmazione;";
            using var con = new MySqlConnection(cs);
            con.Open();
            Console.WriteLine($"mysqlversion:{con.ServerVersion}");
            /*using var cmd  = new MySqlCommand();
            cmd.Connection=con;
            cmd.CommandText="insert into movie(titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produziuone,distributore)values('Midnight Sun','il sole a mezzanotte','2018','2','91','USA','open road films')";

            try{
            cmd.ExecuteNonQuery();
            Console.WriteLine("La riga è stata inserita");
            }
            catch(Exception e)
            {
                Console.WriteLine("Errore "+ e.ToString());
            }*/

            /*var sql="insert into movie(titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produziuone,distributore)values(@titolo_originale,@titolo_italiano,@anno_produzione,@regia_ID,@durata,@paese_produziuone,@distributore)";
            using var cmd= new MySqlCommand(sql,con);
            cmd.Parameters.AddWithValue("@titolo_originale","The Hangover");
            cmd.Parameters.AddWithValue("@titolo_italiano","Una notte da leoni");
            cmd.Parameters.AddWithValue("@anno_produzione","2009");
            cmd.Parameters.AddWithValue("@regia_ID","6");
            cmd.Parameters.AddWithValue("@durata","99");
            cmd.Parameters.AddWithValue("@paese_produziuone","USA");
            cmd.Parameters.AddWithValue("@distributore","Warner bros italia");

            cmd.Prepare();
            try{
            cmd.ExecuteNonQuery();
            Console.WriteLine("La riga è stata inserita");
            }
            catch(Exception e)
            {
                Console.WriteLine("Errore "+ e.ToString());
            }

             string[,] arrayFilm = {{"Batman begins", "Batman begins","2005", "7", "140","USA","Warner Bros"},{"The dark knight","Il cavaliere oscuro","2008","7","152","USA","Warner Bros"},{"The dark knight rises","Il cavaliere oscuro il ritorno","2012","7","164","USA","Warner Bros"},{"Night at the museum","Una notte al museo","2006","8","108","USA","20th centuryfox"},{"Night at the Museum: Battle of the Smithsonian","Una notte al museo 2 - La fuga","2009","8","105","USA","20th centuryfox"}};//matrice
             
             for (int j=0;j<5;j++)
             {
                var sql="insert into movie(titolo_originale,titolo_italiano,anno_produzione,regia_ID,durata,paese_produziuone,distributore)values(@titolo_originale,@titolo_italiano,@anno_produzione,@regia_ID,@durata,@paese_produziuone,@distributore)";
                using var cmd= new MySqlCommand(sql,con);
                cmd.Parameters.AddWithValue("@titolo_originale",arrayFilm[j,0]);
                cmd.Parameters.AddWithValue("@titolo_italiano",arrayFilm[j,1]);
                cmd.Parameters.AddWithValue("@anno_produzione",arrayFilm[j,2]);
                cmd.Parameters.AddWithValue("@regia_ID",arrayFilm[j,3]);
                cmd.Parameters.AddWithValue("@durata",arrayFilm[j,4]);
                cmd.Parameters.AddWithValue("@paese_produziuone",arrayFilm[j,5]);
                cmd.Parameters.AddWithValue("@distributore",arrayFilm[j,6]);

                cmd.Prepare();
            try{
            cmd.ExecuteNonQuery();
            Console.WriteLine("La riga è stata inserita");
            }
            catch(Exception e)
            {
                Console.WriteLine("Errore "+ e.ToString());
            }
            }*/
            TextReader reader1 = new StreamReader("attori.csv");
            var csvReader1 = new CsvReader(reader1, new System.Globalization.CultureInfo("en-US"));
            var actorsfromcsv = csvReader1.GetRecords<Actor>();
            foreach (Actor actorfromcsv in actorsfromcsv)
            {
                
            Console.WriteLine(" " + actorfromcsv.Name + " " + actorfromcsv.Surname + " " + actorfromcsv.Year );

            }  

            var sql = "insert into actors(name,surname,year,fiscalcode,role)values(@name,@surname,@year,@fiscalcode,@role)";
            using var cmd = new MySqlCommand(sql, con);

            cmd.Parameters.AddWithValue("@name", " ");
            cmd.Parameters.AddWithValue("@surname", " ");
            cmd.Parameters.AddWithValue("@year", " ");
            cmd.Parameters.AddWithValue("@fiscalcode", " ");
            cmd.Parameters.AddWithValue("@role", " ");

            cmd.Prepare();
            try
            {
                cmd.ExecuteNonQuery();
                Console.WriteLine("La riga è stata inserita");
            }
            catch (Exception e)
            {
                Console.WriteLine("Errore " + e.ToString());
            }


        }




    }
    class Person
    {
        private string _indirizzo;       
        private string _name;
        public string Name
        {
            get => _name;
            set => _name = value;
        }
        public string Surname { get; set; }
        public int Year { get; set; }
        private string _fiscalcode;  // the name field
        private int _eta;  // the name field        
        public string FiscalCode    // the Name property
        {
            get=>_fiscalcode;
            set=>_fiscalcode = value;
        }
    public string StampaMessaggio(string messaggino)
    {
        //questa è una virtual
        return "questo è il metodo della persona" + messaggino;
    }
    protected int Age()    // the Name property
    {
        //get => _fiscalcode;
        return (2021 - (Year));
    }
    public string nomeCompleto()
    {
        return Name + "  " + Surname + " " + _fiscalcode + " " + Age();
    }
}
class Actor : Person
    {
        public string Role { get; set; }        public string rolePlaying()
        {
            return "the actor's name is " + Name + "  " + Surname + " " + Role + Age();
        }
        enum ActorType
        {
            None,
            Protagonist,
            Coprotagonist,
            VoiceActor
        }
        /* public override string StampaMessaggio()
         {
             //c'è un override
             return "questo è il metodo dell'attore ";
         }*/
    }

}

